<?php

namespace AB\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BlogEditType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->remove('date');
    $builder->remove('image');
  }

  public function getParent()
  {
    return BlogType::class;
  }
}
