<?php
    
    namespace AB\BlogBundle\Controller;
    
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
    use AB\BlogBundle\Entity\Blog;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\FormType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use AB\BlogBundle\Form\BlogType;
    use AB\BlogBundle\Form\BlogEditType;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
    use AB\BlogBundle\BackupMySQL;
    
    class AdminController extends Controller
    {
        public function indexAction(Request $request)
        {
            // Si le visiteur est déjà identifié, on affiche le panneau admin
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $listearticles = $this->getDoctrine()
                ->getManager()
                ->getRepository('ABBlogBundle:Blog')
                ->findBy(array(), array('date' => 'desc'))
                ;
                
            $listemessages = $this->getDoctrine()
                ->getManager()
                ->getRepository('ABBlogBundle:Contact')
                ->findBy(array('new'=>true))
                ;
            $nbmessages=count($listemessages);
            if($nbmessages>=1){
                $request->getSession()->getFlashBag()->add('nbmessages', 'Vous avez '.$nbmessages.' message(s) non lu(s).');
                }
            $listemessages = $this->getDoctrine()
                ->getManager()
                ->getRepository('ABBlogBundle:Contact')
               ->findBy(array(), array('id' => 'desc'))
                ;
            return $this->render('ABBlogBundle:Admin:index.html.twig',array(
                                                                            'listearticles'=>$listearticles,
                                                                            'listemessages'=>$listemessages));
            
            
            }

        // Le service authentication_utils permet de récupérer le nom d'utilisateur
        // et l'erreur dans le cas où le formulaire a déjà été soumis mais était invalide
        // (mauvais mot de passe par exemple)
            $authenticationUtils = $this->get('security.authentication_utils');

            return $this->render('ABBlogBundle:Admin:login.html.twig', array(
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
            ));
        }
        /**
        * @Security("has_role('ROLE_ADMIN')")
        */
        public function ajouterAction(Request $request)
        {
                //On crée un objet Blog
                $blog = new Blog();
                //On crée le Formulaire
                $form = $this->createForm(BlogType::class, $blog);
                
                // Si la requête est en POST
                if ($request->isMethod('POST')) {
                    // On fait le lien Requête <-> Formulaire
                    // À partir de maintenant, la variable $blog contient les valeurs entrées dans le formulaire par le visiteur
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        //$blog->getImage()->upload();
                        // On enregistre notre objet $blog dans la base de données, par exemple
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($blog);
                        $em->flush();
                        
                        $request->getSession()->getFlashBag()->add('notice', 'Article bien enregistré.');
                        
                        // On redirige vers la page de visualisation de l'article nouvellement créée
                        return $this->redirectToRoute('ab_blog_view', array('id' => $blog->getId()));
                    }
                }
                //Sinon on affiche le formulaire
                return $this->render('ABBlogBundle:Admin:add.html.twig',array(
                'form'=>$form->createView(),
                ));
        }
        /**
        * @Security("has_role('ROLE_ADMIN')")
        */        
        public function editerAction(Request $request, $id)
        {
                //On récupère l'objet depuis la bdd
                $blog = $this->getDoctrine()
                ->getManager()
                ->getRepository('ABBlogBundle:Blog')
                ->find($id)
                ;
                if(null===$blog)
                {
                    throw new NotFoundHttpException("L'article d'id ".$id." n'existe pas.");
                }
                $form = $this->createForm(BlogEditType::class, $blog);
                
                if ($request->isMethod('POST')) {
                    // À partir de maintenant, la variable $blog contient les valeurs entrées dans le formulaire par le visiteur
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        // On enregistre notre objet $blog dans la base de données
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($blog);
                        $em->flush();
                        
                        $request->getSession()->getFlashBag()->add('notice', 'Article bien enregistrée.');
                        
                        // On redirige vers la page de visualisation de l'article modifié
                        return $this->redirectToRoute('ab_blog_view', array('id' => $blog->getId()));
                    }
                }
                if($blog->getImage()!==null){
                $blog->getImage()->setUrl('/'.$blog->getImage()->getUploadDir().'/'.$blog->getImage()->getUrl());//remplace l'url de l'image par l'addresse relative au navigateur
                }
                //Sinon on affiche le formulaire
                return $this->render('ABBlogBundle:Admin:edit.html.twig',array(
                'form'=>$form->createView(),'article'=>$blog,
                ));
        }
        /**
        * @Security("has_role('ROLE_ADMIN')")
        */        
        public function supprimerAction(Request $request, $id)
        {
        $blog = $this->getDoctrine()
                ->getManager()
                ->getRepository('ABBlogBundle:Blog')
                ->find($id)
                ;
        $em = $this->getDoctrine()->getManager();
        $em->remove($blog);
        $em->flush();
        $request->getSession()->getFlashBag()->add('notice', "L'article a bien été supprimé.");
        return $this->redirectToRoute('ab_blog_admin');
        }
        
        /**
        * @Security("has_role('ROLE_ADMIN')")
        */        
        public function supprimermessageAction(Request $request, $id)
        {
        $blog = $this->getDoctrine()
                ->getManager()
                ->getRepository('ABBlogBundle:Contact')
                ->find($id)
                ;
        $em = $this->getDoctrine()->getManager();
        $em->remove($blog);
        $em->flush();
        $request->getSession()->getFlashBag()->add('info', "Le message a bien été supprimé.");
        return $this->redirectToRoute('ab_blog_admin');
        }
        
        /**
        * @Security("has_role('ROLE_ADMIN')")
        */        
        public function mailAction(Request $request, $id)
        {
        $contact = $this->getDoctrine()
                ->getManager()
                ->getRepository('ABBlogBundle:Contact')
                ->find($id)
                ;
        $contact->setNew(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();
        
        return $this->redirect('https://mail.google.com/mail/u/0/?view=cm&tf=1&to='.$contact->getEmail().'&cc&bcc&su=Réponse+à+votre+message+sur+ArkenUrbex&body=En réponse à votre message envoyé le '.date_format($contact->getDateEnvoi(),'d/m/Y').'&fs=1');
                
                 return $this->redirectToRoute('ab_blog_admin');
        }
        
        /**
        * @Security("has_role('ROLE_ADMIN')")
        */        
        public function backupAction(Request $request)
        {
                new BackupMySQL(array(
                                        'username' => 'root',
                                        'passwd' => '',
                                        'dbname' => 'symfony'
                                        ));
                return $this->redirectToRoute('ab_blog_admin');
        }
    }
