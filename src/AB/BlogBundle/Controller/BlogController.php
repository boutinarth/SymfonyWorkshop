<?php

namespace AB\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AB\BlogBundle\Form\ContactType;
use AB\BlogBundle\Entity\Contact;

class BlogController extends Controller
{
    public function indexAction(Request $request)
    {
         if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
         $admin=true;
         }
         else
         {
          $admin=false;    
             }
        $tag = $request->query->get('tag');
        $listArticles = $this->getDoctrine()
      ->getManager()
      ->getRepository('ABBlogBundle:Blog')
      ->findBy(
                array('published' => 1), // Critere
                array('date' => 'desc'),        // Tri
                1,                              // Limite
                0                               // Offset
                )
    ;
        $url = $this->get('router')->generate('ab_blog_home');
        return $this->render('ABBlogBundle:Blog:index.html.twig',array(
      'listArticle' => $listArticles,'titre'=>'Derniers Articles :','admin' => $admin
    ));
    }
    public function viewAction($id)
  {
      if($id===0){
        return $this->redirectToRoute('ab_blog_home');
        }
        else{
    $em = $this->getDoctrine()->getManager();

    // Pour récupérer un seul article, on utilise la méthode find($id)
    $article = $em->getRepository('ABBlogBundle:Blog')->find($id);
    $article->increaseVues();
    $em->persist($article);
    $em->flush();
    // $article est donc une instance de A\PlatformBundle\Entity\Blog
    // ou null si l'id $id n'existe pas, d'où ce if :
    if (null === $article) {
      throw new NotFoundHttpException("L'article d'id ".$id." n'existe pas.");
    }
    //$article->getImage()->setUrl('/'.$article->getImage()->getUploadDir().'/'.$article->getImage()->getUrl());//remplace l'url de l'image par l'addresse relative au navigateur
    return $this->render('ABBlogBundle:Blog:view.html.twig', array(
      'article'           => $article
    ));
        }
  }

    public function recentAction()
  {
    // On récupère les données issues de la bdd
    $listArticles = $this->getDoctrine()
      ->getManager()
      ->getRepository('ABBlogBundle:Blog')
      ->findBy(
                array('published' => 1), // Critere
                array('date' => 'desc'),        // Tri
                5,                              // Limite
                0                               // Offset
                )
    ;

    return $this->render('ABBlogBundle:Blog:menu.html.twig', array(
      'listArticle' => $listArticles,'titre'=>'Derniers Articles :'
    ));
  }
  
  public function populaireAction()
  {
    // On récupère les données issues de la bdd
    $listArticles = $this->getDoctrine()
      ->getManager()
      ->getRepository('ABBlogBundle:Blog')
      ->findBy(
                array('published' => 1), // Critere
                array('nbvues' => 'desc'),        // Tri
                5,                              // Limite
                0                               // Offset
                )
    ;

    return $this->render('ABBlogBundle:Blog:menu.html.twig', array(
      // Tout l'intérêt est ici : le contrôleur passe
      // les variables nécessaires au template !
      'listArticle' => $listArticles,'titre'=>'Articles populaires :'
    ));
  }
  
   public function categorieAction($categorie)
  {
    $checkCate = array('Surface','Souterrain','Catacombes','Se lancer');
    if(!in_array($categorie, $checkCate))
    {
        return $this->redirectToRoute('ab_blog_home');
    }
    else
    {
       // On récupère les données issues de la bdd
    $listArticles = $this->getDoctrine()
      ->getManager()
      ->getRepository('ABBlogBundle:Blog')
      ->findBy(
                array('published' => 1,'categorie'=> $categorie), // Critere
                array('date' => 'desc'),        // Tri
                50,                              // Limite
                0                               // Offset
                )
    ;
    //$article->getImage()->setUrl('/'.$article->getImage()->getUploadDir().'/'.$article->getImage()->getUrl());//remplace l'url de l'image par l'addresse relative au navigateur
    return $this->render('ABBlogBundle:Blog:categorie.html.twig', array(
      'listArticle' => $listArticles,'categorie' => $categorie,
    ));
    }
  }
  
  public function aboutAction(Request $request)
  {
    return $this->render('ABBlogBundle:Blog:about.html.twig');
  }
  
  public function contactAction(Request $request)
  {
  
                //On crée un objet Contact
                $contact= new Contact();
                //On crée le Formulaire
                $form = $this->createForm(ContactType::class, $contact);                
                // Si la requête est en POST
                if ($request->isMethod('POST')) {
                    // On fait le lien Requête <-> Formulaire
                    // À partir de maintenant, la variable $contact contient les valeurs entrées dans le formulaire par le visiteur
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        $contact->setMessage(nl2br($contact->getMessage()));
                        //$blog->getImage()->upload();
                        // On enregistre notre objet $blog dans la base de données, par exemple
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($contact);
                        $em->flush();
                        
                        $request->getSession()->getFlashBag()->add('contact', 'Votre message a bien été envoyé. Nous y répondrons dans les plus brefs délais.');
                        return $this->redirectToRoute('ab_blog_contact');
                    }
                }
                //Sinon on affiche le formulaire
                return $this->render('ABBlogBundle:Blog:contact.html.twig',array(
                'form'=>$form->createView(),
                ));
  }
}
