<?php

namespace AB\CVBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AB\BlogBundle\Form\ContactType;
use AB\BlogBundle\Entity\Contact;

class CVController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('ABCVBundle:CV:index.html.twig');
    }
    public function cvAction(Request $request)
    {
        return $this->render('ABCVBundle:CV:cv.html.twig');
    }
    public function realisationsAction(Request $request)
    {
        $listSites = $this->getDoctrine()
      ->getManager()
      ->getRepository('ABCVBundle:Realisation')
      ->findBy(
                array(), // Critere
                array('id' => 'desc')
                )//Tri
    ;
    return $this->render('ABCVBundle:CV:realisations.html.twig',array(
      'listSites' => $listSites));
    }
    public function contactAction(Request $request)
    {
        //On crée un objet Contact
                $contact= new Contact();
                //On crée le Formulaire
                $form = $this->createForm(ContactType::class, $contact);                
                // Si la requête est en POST
                if ($request->isMethod('POST')) {
                    // On fait le lien Requête <-> Formulaire
                    // À partir de maintenant, la variable $contact contient les valeurs entrées dans le formulaire par le visiteur
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        $contact->setMessage(nl2br($contact->getMessage()));
                        //$blog->getImage()->upload();
                        // On enregistre notre objet $blog dans la base de données, par exemple
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($contact);
                        $em->flush();
                        
                        $request->getSession()->getFlashBag()->add('contact', 'Votre message a bien été envoyé. J\'y répondrais dans les plus brefs délais.');
                        return $this->redirectToRoute('ab_contact');
                    }
                }
                //Sinon on affiche le formulaire
                return $this->render('ABCVBundle:CV:contact.html.twig',array(
                'form'=>$form->createView(),
                ));
    }
}
